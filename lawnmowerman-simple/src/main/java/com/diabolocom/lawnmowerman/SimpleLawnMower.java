package com.diabolocom.lawnmowerman;

import com.diabolocom.lawnmowerman.model.Cruise;
import com.diabolocom.lawnmowerman.model.Grid;
import com.diabolocom.lawnmowerman.model.MoveCommand;
import com.diabolocom.lawnmowerman.model.Orientation;
import com.diabolocom.lawnmowerman.model.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by nicos on 12/29/2016.
 */
public final class SimpleLawnMower {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleLawnMower.class);

    public Position move(final int id, final Grid grid, final Cruise cruise) {
        final Position start = cruise.getStart();
        LOG.info("Lawnmover #{} starts at [{},{}, {}] on a {}x{} grid",
                id,
                cruise.getStart().getX(),
                cruise.getStart().getY(),
                cruise.getStart().getOrientation().toChar(),
                grid.getWidth(),
                grid.getHeight());
        Position currentPos = new Position(start);
        for (MoveCommand c : cruise.getMoveCommands()) {
            final Position next;
            switch (c) {
                case forward:
                    next = goForward(currentPos);
                    break;
                case rotateL:
                    next = new Position(currentPos).setOrientation(
                            rotateCounterclockwise(currentPos.getOrientation()));
                    break;
                case rotateR:
                    next = new Position(currentPos).setOrientation(
                            rotateClockwise(currentPos.getOrientation()));
                    break;
                default:
                    throw new IllegalArgumentException("Not supposed to happen");
            }
            currentPos = isOffGrid(grid, next) ? currentPos : next;
        }
        LOG.info("Lawnmover #{} ends at [{},{}, {}]",
                id,
                currentPos.getX(),
                currentPos.getY(),
                currentPos.getOrientation().toChar());
        return currentPos;
    }

    protected Position goForward(final Position start) {
        final Position next = new Position(start);
        switch (start.getOrientation()) {
            case east:
                next.setX(start.getX() + 1);
                break;
            case north:
                next.setY(start.getY() + 1);
                break;
            case south:
                next.setY(start.getY() - 1);
                break;
            case west:
                next.setX(start.getX() - 1);
        }
        return next;
    }

    protected Orientation rotateClockwise(final Orientation o) {
        switch (o) {
            case north: return Orientation.east;
            case east: return Orientation.south;
            case south: return Orientation.west;
            case west: return Orientation.north;
        }
        throw new IllegalArgumentException("Not reached");
    }

    protected Orientation rotateCounterclockwise(final Orientation o) {
        switch (o) {
            case north: return Orientation.west;
            case east: return Orientation.north;
            case south: return Orientation.east;
            case west: return Orientation.south;
        }
        throw new IllegalArgumentException("Not reached");
    }

    protected boolean isOffGrid(final Grid grid, final Position pos) {
        return pos.getX() > (grid.getWidth() - 1) || pos.getY() > (grid.getHeight() - 1);
    }

}
