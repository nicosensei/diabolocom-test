package com.diabolocom.lawnmowerman;

import static org.junit.Assert.assertEquals;

import com.diabolocom.lawnmowerman.input.ProgramParser;
import com.diabolocom.lawnmowerman.model.Cruise;
import com.diabolocom.lawnmowerman.model.MoveCommand;
import com.diabolocom.lawnmowerman.model.Orientation;
import com.diabolocom.lawnmowerman.model.Program;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by nicos on 12/28/2016.
 */
public final class ProgramParserTest {

    @Test
    public final void testParseSample() throws IOException{
        final ProgramParser parser = new ProgramParser();
        final Program prog = parser.parse(ProgramParserTest.class.getResourceAsStream("/sample-2mowers.txt"));
        assertEquals(6, prog.getGrid().getWidth());
        assertEquals(5, prog.getGrid().getHeight());

        final ArrayList<Cruise>cruises = prog.getCruises();
        assertEquals(2, cruises.size());

        final Cruise c1 = cruises.get(0);
        assertEquals(1, c1.getStart().getX());
        assertEquals(2, c1.getStart().getY());
        assertEquals(Orientation.north, c1.getStart().getOrientation());

        final ArrayList<MoveCommand> commands1 = new ArrayList<>(Arrays.asList(
                new MoveCommand[] {
                        MoveCommand.rotateL,
                        MoveCommand.forward,
                        MoveCommand.rotateL,
                        MoveCommand.forward,
                        MoveCommand.rotateL,
                        MoveCommand.forward,
                        MoveCommand.rotateL,
                        MoveCommand.forward,
                        MoveCommand.forward
                }
        ));
        assertEquals(commands1, c1.getMoveCommands());

        final Cruise c2 = cruises.get(1);
        assertEquals(3, c2.getStart().getX());
        assertEquals(3, c2.getStart().getY());
        assertEquals(Orientation.east, c2.getStart().getOrientation());

        final ArrayList<MoveCommand> commands2 = new ArrayList<>(Arrays.asList(
                new MoveCommand[] {
                        MoveCommand.forward,
                        MoveCommand.forward,
                        MoveCommand.rotateR,
                        MoveCommand.forward,
                        MoveCommand.forward,
                        MoveCommand.rotateR,
                        MoveCommand.forward,
                        MoveCommand.rotateR,
                        MoveCommand.rotateR,
                        MoveCommand.forward
                }
        ));
        assertEquals(commands2, c2.getMoveCommands());
    }

}
