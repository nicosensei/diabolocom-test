package com.diabolocom.lawnmowerman;

import com.diabolocom.lawnmowerman.input.ProgramParser;
import com.diabolocom.lawnmowerman.model.Cruise;
import com.diabolocom.lawnmowerman.model.MoveCommand;
import com.diabolocom.lawnmowerman.model.Orientation;
import com.diabolocom.lawnmowerman.model.Position;
import com.diabolocom.lawnmowerman.model.Program;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by nicos on 12/28/2016.
 */
public final class SimpleLawnMowerTest {

    @Test
    public final void testSampleProgram() throws IOException{
        final ProgramParser parser = new ProgramParser();
        final Program prog = parser.parse(SimpleLawnMowerTest.class.getResourceAsStream("/sample-2mowers.txt"));

        final SimpleLawnMower mover = new SimpleLawnMower();

        final Position end1 = mover.move(1, prog.getGrid(), prog.getCruises().get(0));
        assertEquals(1, end1.getX());
        assertEquals(3, end1.getY());
        assertEquals(Orientation.north, end1.getOrientation());

        final Position end2 = mover.move(2, prog.getGrid(), prog.getCruises().get(1));
        assertEquals(5, end2.getX());
        assertEquals(1, end2.getY());
        assertEquals(Orientation.east, end2.getOrientation());
    }

    @Test
    public final void testOffGrid() throws IOException{
        final ProgramParser parser = new ProgramParser();
        final Program prog = parser.parse(SimpleLawnMowerTest.class.getResourceAsStream("/sample-test_off_grid.txt"));

        final SimpleLawnMower mover = new SimpleLawnMower();

        final Position end1 = mover.move(1, prog.getGrid(), prog.getCruises().get(0));
        assertEquals(0, end1.getX());
        assertEquals(1, end1.getY());
        assertEquals(Orientation.north, end1.getOrientation());
    }

}
