# README #

The code is written in Java, and built with Maven. 

The Maven project is split into a couple of modules:

- *lawnmowerman-model* contains simple POJOs tha represent to model of the problem
- *lawnmowerman-input* contains a parser for the specified program files
- *lawnmowerman-simple* contains the simple implementation that moves each lawnmower one after the other, withouth collision handling. It also contains unit tests for the parser and this simple implementation.

To build the project, simply navigate to the project root and run `mvn -U clean install`. which will also run init tests.