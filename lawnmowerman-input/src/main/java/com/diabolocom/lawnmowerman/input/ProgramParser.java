package com.diabolocom.lawnmowerman.input;

import com.diabolocom.lawnmowerman.model.Cruise;
import com.diabolocom.lawnmowerman.model.Grid;
import com.diabolocom.lawnmowerman.model.MoveCommand;
import com.diabolocom.lawnmowerman.model.Orientation;
import com.diabolocom.lawnmowerman.model.Position;
import com.diabolocom.lawnmowerman.model.Program;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nicos on 12/28/2016.
 */
public final class ProgramParser {

    public static final Charset UTF8 = Charset.forName("UTF-8");

    private enum LineFormat {
        grid(Pattern.compile("(\\d)\\s+(\\d)")),
        mowerStartPosition(Pattern.compile("(\\d)\\s+(\\d)\\s+([NESWnesw])")),
        mowerMoves(Pattern.compile("[RLFrlf]+"));
        private final Pattern pattern;

        LineFormat(final Pattern pattern) {
            this.pattern = pattern;
        }
    }

    /**
     * Parses an input file as {@link Program} bean using the default UTF-8 charset.
     *
     * @param inputStream
     * @return a {@link Program} instance
     */
    public Program parse(final InputStream inputStream) throws IOException {
        return parse(inputStream, UTF8);
    }

    /**
     * Parses an input file as {@link Program} bean.
     *
     * @param inputStream
     * @param charset     the charset to use
     * @return a {@link Program} instance
     */
    public Program parse(final InputStream inputStream, final Charset charset) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, charset));
        try {
            String line = br.readLine();
            if (line == null) {
                throw new IOException("Empty input!");
            }
            Matcher matcher = LineFormat.grid.pattern.matcher(line);
            if (!matcher.matches()) {
                throw new IOException(String.format("Expected first line '%s' to match pattern '%s'",
                        line, LineFormat.grid.pattern.pattern()));
            }

            final Program p = new Program().setGrid(new Grid()
                    .setWidth(Integer.parseInt(matcher.group(1)))
                    .setHeight(Integer.parseInt(matcher.group(2))));

            String startPos;
            String commands;
            while (true) {
                startPos = br.readLine();
                if (startPos == null) {
                    break;
                }

                commands = br.readLine();
                if (commands == null) {
                    break;
                }

                matcher = LineFormat.mowerStartPosition.pattern.matcher(startPos);
                if (!matcher.matches()) {
                    throw new IOException(String.format("Expected line '%s' to match pattern '%s'",
                            line, LineFormat.mowerStartPosition.pattern.pattern()));
                }

                final Cruise cruise = new Cruise().setStart(new Position(
                        Integer.parseInt(matcher.group(1)),
                        Integer.parseInt(matcher.group(2)),
                        Orientation.fromChar(matcher.group(3).toLowerCase().charAt(0))));

                matcher = LineFormat.mowerMoves.pattern.matcher(commands);
                if (!matcher.matches()) {
                    throw new IOException(String.format("Expected line '%s' to match pattern '%s'",
                            line, LineFormat.mowerMoves.pattern.pattern()));
                }

                for (final char c : commands.toUpperCase().toCharArray()) {
                    cruise.addMoveCommand(MoveCommand.forward.fromChar(c));
                }

                p.addCruise(cruise);
            }

            return p;
        } finally {
            br.close();
        }
    }

}
