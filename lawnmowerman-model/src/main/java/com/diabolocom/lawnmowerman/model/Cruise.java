package com.diabolocom.lawnmowerman.model;

import java.util.ArrayList;

/**
 * Created by nicos on 12/28/2016.
 */
public final class Cruise {

    private static final int INITIAL_COMMANDS_SIZE = 10;

    private Position start;

    private ArrayList<MoveCommand> moveCommands = new ArrayList<>(INITIAL_COMMANDS_SIZE);

    public static int getInitialCommandsSize() {
        return INITIAL_COMMANDS_SIZE;
    }

    public Position getStart() {
        return start;
    }

    public Cruise setStart(final Position start) {
        this.start = start;
        return this;
    }

    public ArrayList<MoveCommand> getMoveCommands() {
        return moveCommands;
    }

    public Cruise addMoveCommand(final MoveCommand moveCommand) {
        this.moveCommands.add(moveCommand);
        return this;
    }
}
