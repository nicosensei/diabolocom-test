package com.diabolocom.lawnmowerman.model;

/**
 * Created by nicos on 12/28/2016.
 */
public final class Grid {

    private int width;

    private int height;

    public int getWidth() {
        return width;
    }

    public Grid setWidth(final int width) {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public Grid setHeight(final int height) {
        this.height = height;
        return this;
    }

}
