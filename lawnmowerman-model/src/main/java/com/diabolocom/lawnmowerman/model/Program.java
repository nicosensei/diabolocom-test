package com.diabolocom.lawnmowerman.model;

import java.util.ArrayList;

/**
 * Created by nicos on 12/28/2016.
 */
public final class Program {

    private static final int INITIAL_CRUISES_SIZE = 10;

    private Grid grid;

    private ArrayList<Cruise> cruises = new ArrayList<>(INITIAL_CRUISES_SIZE);

    public Grid getGrid() {
        return grid;
    }

    public ArrayList<Cruise> getCruises() {
        return cruises;
    }

    public Program addCruise(final Cruise cruise) {
        this.cruises.add(cruise);
        return this;
    }

    public Program setGrid(final Grid grid) {
        this.grid = grid;
        return this;
    }

}
