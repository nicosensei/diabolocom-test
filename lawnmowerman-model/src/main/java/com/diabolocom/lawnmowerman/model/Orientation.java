package com.diabolocom.lawnmowerman.model;

/**
 * Created by nicos on 12/28/2016.
 */
public enum Orientation {

    north,
    east,
    south,
    west;

    public char toChar() {
        return name().charAt(0);
    }

    public static Orientation fromChar(final char c) {
        for (Orientation o : values()) {
            if (o.toChar() == c) {
                return o;
            }
        }
        throw new IllegalArgumentException("Unknown orientation " + c);
    }

}
