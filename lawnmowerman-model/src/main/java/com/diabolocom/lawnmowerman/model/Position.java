package com.diabolocom.lawnmowerman.model;

/**
 * Created by nicos on 12/28/2016.
 */
public final class Position {

    private int x;

    private int y;

    private Orientation orientation;

    public int getX() {
        return x;
    }

    public Position(final int x, final int y, final Orientation orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public Position(final Position copyFrom) {
        this(copyFrom.getX(), copyFrom.getY(), copyFrom.getOrientation());
    }

    public Position setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public Position setY(int y) {
        this.y = y;
        return this;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public Position setOrientation(Orientation orientation) {
        this.orientation = orientation;
        return this;
    }

}
