package com.diabolocom.lawnmowerman.model;

/**
 * Created by nicos on 12/28/2016.
 */
public enum MoveCommand {

    forward('F'),
    rotateR('R'),
    rotateL('L');

    private final char aChar;

    MoveCommand(char aChar) {
        this.aChar = aChar;
    }

    public char toChar() {
        return aChar;
    }

    public MoveCommand fromChar(final char c) {
        for (MoveCommand mc : values()) {
            if (mc.toChar() == c) {
                return mc;
            }
        }
        throw new IllegalArgumentException("Unknown move command '" + c + "'");
    }
}
